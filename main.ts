import {GetProductsForIngredient, GetRecipes} from './supporting-files/data-access';
import {ExpectedRecipeSummary, RunTest} from './supporting-files/testing';
import {GetCostPerBaseUnit, GetNutrientFactInBaseUnits, SumUnitsOfMeasure} from './supporting-files/helpers'
import {Product} from './supporting-files/models'

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

recipeData.forEach(_recipe => {
    const _recipeSummary = {
        cheapestCost: 0,
        nutrientsAtCheapestCost: {}
    }
    // Loop Recipe Items
    _recipe.lineItems.forEach(_item => {
        const products = GetProductsForIngredient(_item.ingredient)

        // Loop ingredients - find the cheapest
        const _products: Product[] = []
        products.forEach(product => {
            const selectedSupplier = product.supplierProducts.sort((_supplierProductA, _supplierProductB) => {
                return GetCostPerBaseUnit(_supplierProductB) - GetCostPerBaseUnit(_supplierProductA)
            }).pop()
            if (selectedSupplier) {
                product.supplierProducts = [selectedSupplier]
            }
            _products.push(product)
        })
        const selectedProduct = _products.sort((_productA, _productB) => {
            return GetCostPerBaseUnit(_productB.supplierProducts[0]) - GetCostPerBaseUnit(_productA.supplierProducts[0])
        }).pop()
        if (selectedProduct) {
            selectedProduct.nutrientFacts.map(_nutrientFact => GetNutrientFactInBaseUnits(_nutrientFact)).forEach(_nutrientFact => {
                if (_recipeSummary.nutrientsAtCheapestCost[_nutrientFact.nutrientName]) {
                    _nutrientFact.quantityAmount = SumUnitsOfMeasure(_recipeSummary.nutrientsAtCheapestCost[_nutrientFact.nutrientName].quantityAmount, _nutrientFact.quantityAmount)
                }
                _recipeSummary.nutrientsAtCheapestCost[_nutrientFact.nutrientName] = _nutrientFact
            })
            const _supplierProduct = selectedProduct.supplierProducts.pop();
            if (_supplierProduct) {
                _recipeSummary.cheapestCost += GetCostPerBaseUnit(_supplierProduct) * _item.unitOfMeasure.uomAmount
            }
        }
    })
    _recipeSummary.nutrientsAtCheapestCost = Object.keys(_recipeSummary.nutrientsAtCheapestCost)
        .sort()
        .reduce((accumulator, key) => {
            accumulator[key] = _recipeSummary.nutrientsAtCheapestCost[key];
            return accumulator;
        }, {});
    recipeSummary[_recipe.recipeName] = _recipeSummary
})

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);



// const _recipeSummary = selectedProducts.reduce(({ cheapestCost, nutrientsAtCheapestCost }, product, currentIndex) => {
//     product.nutrientFacts.forEach(_nutrientFact => {
//         if (nutrientsAtCheapestCost[_nutrientFact.nutrientName]) {
//             nutrientsAtCheapestCost[_nutrientFact.nutrientName].quantityAmount.uomAmount += _nutrientFact.quantityAmount.uomAmount
//         } else {
//             nutrientsAtCheapestCost[_nutrientFact.nutrientName] = _nutrientFact
//         }
//     })
//     cheapestCost += product.supplierProducts[0].supplierPrice
//     return {
//         cheapestCost,
//         nutrientsAtCheapestCost,
//     }
// }, {cheapestCost: 0, nutrientsAtCheapestCost: {}})
